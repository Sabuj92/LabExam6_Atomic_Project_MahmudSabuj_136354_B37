<?php
namespace Model;

use PDO;
use PDOException;


class Database
{
    public $conn;
    public $host="localhost";
    public $dbname="atomic_project_b37";
    public $username = "root";
    public $password = "";

    public function __construct()
    {
        try {

            //$this->conn = new PDO("mysql:host=localhost;dbname=atomicprojectb37", $this->username, $this->password);
            $this->conn = new PDO("mysql:host=".$this->host.";dbname=".$this->dbname, $this->username, $this->password);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}