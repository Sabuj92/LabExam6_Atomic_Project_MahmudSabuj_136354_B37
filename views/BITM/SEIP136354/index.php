<?php
require_once ('../../../vendor/autoload.php');

use App\BookTitle;
use App\Birthday;
use App\City;
use App\Email;
use App\Gender;
use App\Hobby;
use App\ProfilePicture;
use App\SummaryOfOrganization;

$Birthday = new Birthday();
$Birthday->index();
echo "<br>";

$BookTitle = new BookTitle();
$BookTitle->index();
echo "<br>";

$City = new City();
$City->index();
echo "<br>";

$Email = new Email();
$Email->index();
echo "<br>";

$Gender = new Gender();
$Gender->index();
echo "<br>";

$Hobbies = new Hobby();
$Hobbies->index();
echo "<br>";

$ProfilePicture = new ProfilePicture();
$ProfilePicture->index();
echo "<br>";

$SummaryOfOrganization = new SummaryOfOrganization();
$SummaryOfOrganization->index();
echo "<br>";
